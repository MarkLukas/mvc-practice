<form action="index.php?action=<?= bereinige($action) ?>" method="post">
    <input
            type="hidden" name="id"
            value="<?= (int)$aufgabe->getId() ?>"
    />
    <select id="verantwortlicheIds" name="verantwortlicheIds">
        <?php foreach ($personen as $person): ?>
            <option name="verantwortlicheIds" value="<?=$person->getId()?>;">
                <?= $person->getVorname(); ?><?= ' ' . $person->getName(); ?>
            </option>
        <?php endforeach; ?>
    </select>
    <input
            type="text" name="zeit" id="zeit" placeholder="Zeit"
            value="<?= bereinige($aufgabe->getZeit()) ?>"
    />
    <input
            type="text" name="beschreibung" id="beschreibung" placeholder="Beschreibung"
            value="<?= (float)$aufgabe->getBeschreibung() ?>"
    />
    <input
            type="hidden" name="erledigt"
            value="<?= (int)$aufgabe->isErledigt() ?>"
    />
    <input
            type="text" name="wiederkehrend" id="wiederkehrend" placeholder="Wiederkehrend"
            value="<?= (float)$aufgabe->getWiederkehrend() ?>"
    />

    <input type="submit" value="speichern" />
</form>


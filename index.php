<?php

require_once 'inc/funktionen.inc.php';
spl_autoload_register('autoloadControllers');
spl_autoload_register('autoloadEntities');
spl_autoload_register('autoloadTraits');

require_once 'inc/datenbank.inc.php';

Buch::verbindeZuDb($db);
Person::verbindeZuDb($db);
Aufgabe::verbindeZuDb($db);

$controller = isset($_GET['controller']) ? $_GET['controller'] : 'index';
$action = isset($_GET['action']) ? $_GET['action'] : 'index';
/*
 * momentan IMMER IndexController
 *
 */
$controllerName = ucfirst($controller) . 'Controller';

if(class_exists($controllerName)){
    $requestController = new $controllerName();
    $requestController->run($action);
} else{
    $requestController = new IndexController();
    $requestController->render404();
}



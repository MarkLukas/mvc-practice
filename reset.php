<?php

require_once  'inc/datenbank.inc.php';

//falls die Tabelle schon existiert, wegräumen
$db->query('DROP TABLE IF EXISTS buecher');

$db->query(
    'CREATE TABLE buecher(
        id INTEGER PRIMARY KEY AUTO_INCREMENT,   
        titel VARCHAR(255),
        preis DECIMAL(6,2)
    ) DEFAULT CHARSET = utf8'
);

$db->query(
    'INSERT INTO buecher (titel, preis) VALUES
    ("Der Graf von Monte Christo", 9.95),
    ("Total Recall", 7.95)
    '
);
$db->query('DROP TABLE IF EXISTS personen');
$db->query( 'CREATE TABLE personen(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    vorname VARCHAR(255),
    rechte VARCHAR(50)
) DEFAULT CHARSET = utf8'
  //Person
);
$db->query(
    'INSERT INTO personen (name, vorname, rechte) VALUES
    ("Mehl", "Andreas", "developer"),
    ("Lukas", "Mark", "developer")
    '
);


$db->query('DROP TABLE IF EXISTS aufgaben');

$db->query(
    'CREATE TABLE aufgaben(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    verantwortlicheIds VARCHAR(100),
    zeit DATE,
    beschreibung VARCHAR(255),
    erledigt BIT,
    wiederkehrend VARCHAR(1)
)DEFAULT CHARSET = utf8'
);

$db->query('INSERT INTO aufgaben(verantwortlicheIds, zeit, beschreibung, erledigt, wiederkehrend) VALUES
(";1;",CURRENT_DATE, "AndreasAufgabe", 0, "t" ),
(";1;",CURRENT_DATE, "AndreasAufgabe2", 0, "t" ),
(";2;",CURRENT_DATE, "MarkAufgabe", 0, "t" )
');
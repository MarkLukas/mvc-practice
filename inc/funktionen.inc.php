<?php

function bereinige($benuterEingabe, $encoding = 'UTF-8'){
    return htmlspecialchars(
        strip_tags($benuterEingabe),
        ENT_QUOTES | ENT_HTML5,
        $encoding
    );
}

function redirect($url){
    header('Location: ' . $url);
    exit;
}

function autoloadControllers($controllerName)
{
    $controllerFile = 'src/Controllers/' . $controllerName . '.php';
    if(file_exists($controllerFile)){
        require_once $controllerFile;
    }
}
function autoloadEntities($entityName)
{
    $entityFile = 'src/Entities/' . $entityName . '.php';
    if (file_exists($entityFile)) {
        require_once $entityFile;
    }
}
function autoloadTraits($traitName){
    $traitFile = 'src/Traits/' . $traitName . '.php';
    if(file_exists($traitFile)){
        require_once $traitFile;
    }
}

function debugLog($v){
    $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
    $txt = "debug-Info" . var_export($v);
    fwrite($myfile, $txt);
    fclose($myfile);
}



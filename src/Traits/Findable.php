<?php

trait Findable{
    public static function findeAlle(){
        $sql = vsprintf(
            'SELECT * FROM %s',
            [self::ermittleTable()]
        );
        $abfrage = self::$db->query($sql);
        $abfrage->setFetchMode(PDO::FETCH_CLASS, get_class());
        return $abfrage->fetchAll();
    }

    public static function finde($id){
        $sql = vsprintf(
            'SELECT * FROM %s WHERE id = ?',
             [self::ermittleTable()]
        );
        $abfrage = self::$db->prepare($sql);
        $abfrage->execute([$id]);
        $abfrage->setFetchMode(PDO::FETCH_CLASS, get_class());
        return $abfrage->fetch();
    }

    public function getEigeneAufgaben($id){
        $formatted_id = ";" . $id . ";";
      //  $query = vsprintf("SELECT * FROM aufgaben WHERE verantwortliche_ids LIKE %s", $formatted_id);
      //  $abfrage = self::$db->prepare($query);
        $abfrage = self::$db->prepare('SELECT * FROM aufgaben WHERE verantwortlicheIds LIKE :id');
        $abfrage->bindValue(':id', "%{$formatted_id}%");
        //var_dump($abfrage);
        $abfrage->execute();
        $abfrage->setFetchMode(PDO::FETCH_CLASS, "Aufgabe");
        return $abfrage->fetchAll();

        //in Indexcontroller bei zeigeAction aufrufen
    }
}

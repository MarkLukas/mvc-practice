<?php

class Buch
{
    protected $id = 0;
    protected $titel = '';
    protected $preis = 0; //Nettopreis

    protected static $table = 'buecher';

    use ActiveRecordable, Findable, Persistable, Deletable;



    public function __construct(array $daten = [])
    {
        $this->setDaten($daten);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitel()
    {
        return $this->titel;
    }

    /**
     * @param string $titel
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }

    /**
     * @return int
     */
    public function getPreis()
    {
        return $this->preis;
    }

    /**
     * @param int $preis
     */
    public function setPreis($preis)
    {
        $this->preis = $preis;
    }

    public function getBruttoPreis(){
        $bruttoPreis = $this->getPreis() * 1.07;

        return $bruttoPreis;
    }

    public function setBruttopreis($bruttoPreis){
        $this->setPreis($bruttoPreis / 1.07);
    }

}
<?php


class Person
{
    protected $id = 0;
    protected $name = '';
    protected $vorname = '';
    protected $befugnis = '';

    protected static $table = 'personen';

    use ActiveRecordable, Findable, Persistable, Deletable;



    public function __construct(array $daten = [])
    {
        $this->setDaten($daten);
    }

    /**
     * @return int
     */

    // ID braucht keinen Setter
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getVorname()
    {
        return $this->vorname;
    }

    /**
     * @param string $vorname
     */
    public function setVorname($vorname)
    {
        $this->vorname = $vorname;
    }

    /**
     * @return string
     */
    public function getBefugnis()
    {
        return $this->befugnis;
    }

    /**
     * @param string $befugnis
     */
    public function setBefugnis($befugnis)
    {
        $this->befugnis = $befugnis;
    }


    /**
     *Brauche Funktion, die Alle Aufgaben aus der Tabelle aufgaben holt, die die Id dieser Person beinhaltet
     */
}


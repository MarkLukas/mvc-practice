<?php


class Aufgabe
{
    protected $id = 0;
    protected $verantwortlicheIds = '';
    protected $zeit = ''; //Datetime
    protected $beschreibung = '';
    protected $erledigt = false;
    protected $wiederkehrend = '';

    protected static $table = 'aufgaben';

    use ActiveRecordable, Findable, Persistable, Deletable;


    public function __construct(array $daten = [])
    {
        $this->setDaten($daten);
    }

    /**
     * @return int
     */

    // ID braucht keinen Setter
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getZeit()
    {
        return $this->zeit;
    }

    /**
     * @param string $zeit
     */
    public function setZeit($zeit)
    {
        $this->zeit = $zeit;
    }

    /**
     * @return string
     */
    public function getBeschreibung()
    {
        return $this->beschreibung;
    }

    /**
     * @param string $beschreibung
     */
    public function setBeschreibung($beschreibung)
    {
        $this->beschreibung = $beschreibung;
    }

    /**
     * @return bool
     */
    public function isErledigt()
    {
        return $this->erledigt;
    }

    /**
     * @param bool $erledigt
     */
    public function setErledigt($erledigt)
    {
        $this->erledigt = $erledigt;
    }

    /**
     * @return string
     */
    public function getWiederkehrend()
    {
        return $this->wiederkehrend;
    }

    /**
     * @param string $wiederkehrend
     */
    public function setWiederkehrend($wiederkehrend)
    {
        $this->wiederkehrend = $wiederkehrend;
    }
    /**
     * @return string
     */
    public function getVerantwortlicheIds()
    {
        return $this->verantwortlicheIds;
    }

    /**
     * @param string $verantwortlicheIds
     */
    public function setVerantwortlicheIds($verantwortlicheIds)
    {
        $this->verantwortlicheIds = ";" . $verantwortlicheIds;
    }

}

<?php

class IndexController extends AbstractBase
{

    public function indexAction()
    {
       // $this->addContext('personen',Person::findeAlle());
        $this->addContext('aufgaben', Aufgabe::findeAlle());



    }

    public function zeigeAction(){
       // $this->addContext('personen',Person::findeAlle());
        $this->addContext('person', Person::finde($_GET['id']));
        $this->addContext('aufgaben', Person::getEigeneAufgaben($_GET['id']));
    }

    public function neuAction()
    {
        //adds for the template to use, move into render run function later
        //creates empty Object
        $aufgabe = new Aufgabe();
      //  debugLog($_POST);
        if($_POST){
            debugLog($_POST);
            $aufgabe->setDaten($_POST); //takes post and calls up Setters to set the data
            $aufgabe->speichere();

        }

        $this->addContext('aufgabe', $aufgabe);
        $this->setTemplate('bearbeiteAction');
    }

    protected function bearbeiteAction(){
        $aufgabe = Aufgabe::finde($_REQUEST['id']);
        if($_POST){
            //var_dump($_POST);
            $aufgabe->setDaten($_POST);
            $aufgabe->speichere();
           // redirect('index.php');
        }
        $this->addContext('aufgabe', $aufgabe);
    }
  /*  protected function bearbeiteAction(){
        $buch = Buch::finde($_REQUEST['id']);
        if($_POST){
            $buch->setDaten($_POST);
            $buch->speichere();
            redirect('index.php');
        }
        $this->addContext('buch', $buch);
    } */

    protected function loescheAction()
    {

        $aufgabe = Aufgabe::finde($_GET['id']);
        $aufgabe->loesche();
        redirect('index.php');
    }
  /*  protected function loescheAction()
    {
        $buch = Buch::finde($_GET['id']);
        $buch->loesche();
        redirect('index.php');
    } */



}

<?php


abstract class AbstractBase
{
    protected $context = [];
    protected $template = '';


    public function run($action)
    {
        $this->addContext('personen',Person::findeAlle());
        $this->addContext('action', $action);

        $methodName = $action . 'Action';
        $this->setTemplate($methodName);

        if(method_exists($this, $methodName)){
            $this->$methodName();
        }else{
            $this->render404();
        }
        $this->render();
    }

    public function render404(){
        header('HTTP/1.0 404 Not Found');
        die('Error 404');
    }
    /**
     * @param string $template
     */
    protected function setTemplate($template, $controller = null)
    {
        if(empty($controller)){
            $controller = get_class($this);
        }
        $this->template = $controller . '/' . $template . '.tpl.php';
    }
    /**
     * @return string
     */
    protected function getTemplate()
    {
        return $this->template;
    }

    protected function addContext($key, $value)
    {
        $this->context[$key] = $value;
    }

    protected function render()
    {
        extract($this->context);
        $template = $this->getTemplate();
        require_once 'templates/layout.tpl.php';
    }

}